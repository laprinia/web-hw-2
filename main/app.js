function addTokens(input, tokens){

let output=input
    if(typeof(input)!="string")
        {
        throw new Error("Invalid input")
         }
       else if (input.length<6)
       {
         throw new Error("Input should have at least 6 characters")


      }else if (!(input.indexOf('...')>-1)) {
        return input
      }
       else {

         Object.keys(tokens).forEach((key,index)=>
    {

      output=output.replace('...',JSON.stringify((`${tokens[key]}`)))
    })
  }
   return output;
}

const app = {
    addTokens: addTokens
}

module.exports = app;
